Name:		perl-Business-ISBN
Version:        3.009
Release:        2
Summary:        Work with International Standard Book Numbers
License:        Artistic-2.0
URL:            https://metacpan.org/release/Business-ISBN
Source0:        https://cpan.metacpan.org/authors/id/B/BD/BDFOY/Business-ISBN-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  coreutils findutils make perl-interpreter perl-generators
BuildRequires:  perl(ExtUtils::MakeMaker) perl(Business::ISBN::Data) >= 20230322.001

#Test
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Pod)
Requires:       perl(GD::Barcode::EAN13)

%description
There are three modules in this package:
 * Business::ISBN - work with International Standard Book Numbers
 * Business::ISBN10 - work with 10 digit International Standard Book Numbers
 * Business::ISBN13 - work with 13 digit International Standard Book Numbers

%package_help

%prep
%autosetup -n Business-ISBN-%{version} -p1

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%doc Changes README.pod
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 3.009-2
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jul 23 2024 gss <guoshengsheng@kylinos.cn> - 3.009-1
- Upgrade to version 3.009
- refresh distro, update email addresses, move to BRIANDFOY

* Wed Jul 26 2023 xujing <xujing125@huawei.com> - 3.008-1
- update version to 3.008

* Fri Oct 21 2022 xujing <xujing125@huawei.com> - 3.007-1
- update version to 3.007

* Mon Nov 22 2021 shixuantong <shxuantong@huawei.com> - 3.006-1
- update version to 3.006 and update Source0

* Mon Feb 10 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.005-1
- initial package
